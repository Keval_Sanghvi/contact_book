-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2021 at 11:58 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `contact_book`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `first_name` varchar(35) NOT NULL,
  `last_name` varchar(35) NOT NULL,
  `birthdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `first_name`, `last_name`, `birthdate`) VALUES
(1, 'keval', 'sanghvi', '2021-03-17'),
(2, 'dhruvi', 'sanghvi', '2021-03-17'),
(16, 'viraj', 'sanghvi', '2021-03-17'),
(17, 'raj', 'sanghvi', '2021-03-17'),
(19, 'manan', 'savla', '2021-03-17'),
(25, 'darshil', 'mehta', '2009-01-10');

-- --------------------------------------------------------

--
-- Table structure for table `email_id`
--

CREATE TABLE `email_id` (
  `id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `email` varchar(35) NOT NULL,
  `primary_email` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_id`
--

INSERT INTO `email_id` (`id`, `contact_id`, `email`, `primary_email`) VALUES
(43, 1, 'sanghvi.k@somaiya.edu', 0),
(44, 1, 'kevalsanghvi@gmail.com', 0),
(45, 1, 'kevalssanghvi100@gmail.com', 1),
(46, 2, 'dhruvi@gmail.com', 0),
(47, 2, 'dhruvi1@gmail.com', 0),
(48, 2, 'dhruvi2@gmail.com', 1),
(49, 16, 'viraj11@gmail.com', 0),
(50, 16, 'v123@gmail.com', 1),
(51, 17, 'raj11@gmail.com', 1),
(54, 25, 'darshil@gmail.com', 0),
(55, 25, 'darshil@somaiya.edu', 1),
(56, 19, 'manansavla@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `phone_number`
--

CREATE TABLE `phone_number` (
  `id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `primary_number` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_number`
--

INSERT INTO `phone_number` (`id`, `contact_id`, `phone`, `primary_number`) VALUES
(54, 1, '9768387175', 1),
(55, 2, '2222222222', 0),
(56, 2, '9004009254', 1),
(57, 16, '3131313131', 0),
(58, 16, '1131131131', 1),
(59, 16, '1212313111', 0),
(60, 17, '2222222222', 1),
(65, 25, '2564002911', 1),
(66, 25, '1231451312', 0),
(67, 19, '0101010123', 0),
(68, 19, '1012131012', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_id`
--
ALTER TABLE `email_id`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_id_ibfk_1` (`contact_id`);

--
-- Indexes for table `phone_number`
--
ALTER TABLE `phone_number`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phone_number_ibfk_1` (`contact_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `email_id`
--
ALTER TABLE `email_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `phone_number`
--
ALTER TABLE `phone_number`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `email_id`
--
ALTER TABLE `email_id`
  ADD CONSTRAINT `email_id_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `phone_number`
--
ALTER TABLE `phone_number`
  ADD CONSTRAINT `phone_number_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

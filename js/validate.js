function validateForm(e) {
    if ($("#first_name").val() == "") {
        $(".first_name_error").text("Please Fill Out This Information!");
        setTimeout(() => {
            callMe($(".first_name_error"));
        }, 5000);
        return false;
    }
    if ($("#first_name").val().length < 2 || $("#first_name").val().length > 35) {
        $(".first_name_error").text("Please Add Data of Valid Length Between 2 and 35 Characters!");
        setTimeout(() => {
            callMe($(".first_name_error"));
        }, 5000);
        return false;
    }
    if ($("#last_name").val() == "") {
        $(".last_name_error").text("Please Fill Out This Information!");
        setTimeout(() => {
            callMe($(".last_name_error"));
        }, 5000);
        return false;
    }
    if ($("#last_name").val().length < 2 || $("#last_name").val().length > 35) {
        $(".last_name_error").text("Please Add Data of Valid Length Between 2 and 35 Characters!");
        setTimeout(() => {
            callMe($(".last_name_error"));
        }, 5000);
        return false;
    }
    if ($(".numbers input[type=tel]").val().length != 10) {
        $(".telephone_error").text("Length must be 10 characters!");
        setTimeout(() => {
            callMe($(".telephone_error"));
        }, 5000);
        return false;
    }
    if ($(".emails input[type=email]").val().length == 0) {
        $(".email_error").text("Please Fill Out This Information!");
        setTimeout(() => {
            callMe($(".email_error"));
        }, 5000);
        return false;
    }
    let f = true;
    if(f) {
        let elems = $(".numbers input[type=tel]");
        let count = 0;
        $(".numbers input[type=tel]").each(function() {
            if($(this).val() == elems.val()) {
                count += 1;
            }
            if(count>1) {
                $(".telephone_error").text("Same Phone Numbers Are Not Allowed! Change Or Remove Same Phone Numbers!");
                setTimeout(() => {
                    callMe($(".telephone_error"));
                }, 5000);
                f = false;
            }
        });
        if(!f) {
            return f;
        }
    }
    f = true;
    if(f) {
        let elems1 = $(".emails input[type=email]");
        let count1 = 0;
        $(".emails input[type=email]").each(function() {
            if($(this).val() == elems1.val()) {
                count1 += 1;
            }
            if(count1>1) {
                $(".email_error").text("Same Email-Id's Are Not Allowed! Change Or Remove Same Email-Id's!");
                setTimeout(() => {
                    callMe($(".email_error"));
                }, 5000);
                f = false;
            }
        });
        return f;
    }
}

$(function () {
    $("#first_name").change(function () {
        $(this).val($(this).val().trim());
    });
    $("#last_name").change(function () {
        $(this).val($(this).val().trim());
    });
    $(".numbers").on('change', 'input[type=tel]', function () {
        $(this).val($(this).val().trim());
        if (!/^\d+$/.test(this.value)) {
            $(this).siblings(".telephone_error").text("Enter Only Digits!");
            setTimeout(() => {
                callMe($(this).siblings(".telephone_error"));
            }, 5000);
        } else if (this.value.length != 10) {
            $(this).siblings(".telephone_error").text("Length of Number should be 10 Characters!");
            setTimeout(() => {
                callMe($(".telephone_error"));
            }, 5000);
        }
        let elems = $(".numbers input[type=tel]");
        let count = 0;
        $(".numbers input[type=tel]").each(function() {
            if($(this).val() == elems.val()) {
                count += 1;
            }
            if(count>1) {
                $(".telephone_error").text("Same Phone Numbers Are Not Allowed! Change Or Remove Same Phone Numbers!");
                setTimeout(() => {
                    callMe($(".telephone_error"));
                }, 5000);
            }
        });
    });
    $(".emails").on('change', 'input[type=email]', function () {
        $(this).val($(this).val().trim());
        if (!/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.value)) {
            $(this).siblings(".email_error").text("Enter Valid Email!");
            setTimeout(() => {
                callMe($(this).siblings(".email_error"));
            }, 5000);
        }
        let elems = $(".emails input[type=email]");
        let count = 0;
        $(".emails input[type=email]").each(function() {
            if($(this).val() == elems.val()) {
                count += 1;
            }
            if(count>1) {
                $(".email_error").text("Same Email-Id's Are Not Allowed! Change Or Remove Same Email-Id's!");
                setTimeout(() => {
                    callMe($(".email_error"));
                }, 5000);
            }
        });
    });
});

function callMe($element) {
    $element.text("");
}
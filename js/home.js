$(function() {
    $(".delete-button").click(function() {
        let id = $(this).data('id');
        $("#modal-agree-button").attr('href', `delete-contact.php?id=${id}`);
    });
    
    $(".edit-button").click(function() {
        let id1 = $(this).data('id');
        location.href = `edit-contact.php?id=${id1}`;
    });

    function getUrlVars() {
        var vars = [], hash;
        var url = window.location.href;
        var queryString = url.slice(url.indexOf('?') + 1);
        var hashes = queryString.split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    
    var queryStrings = getUrlVars();
    var q = queryStrings['q'];
    var op = queryStrings['op'];
    if( q === 'success' && op === 'insert') {
        $(".alert-message").text("Successfully Inserted!!!");
        $(".alert-message").removeClass("d-none");
        $(".alert-message").fadeOut(5000);
    } else if( q === 'success' && op === 'edit') {
        $(".alert-message").text("Successfully Edited!!!");
        $(".alert-message").removeClass("d-none");
        $(".alert-message").fadeOut(5000);
    }
});
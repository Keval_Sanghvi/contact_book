let addButtonNumber = $(".plus-number");
let addButtonEmail = $(".plus-email");
var incrPhone = 0;
var incrEmail = 0;
addButtonNumber.click(function (e) {
    e.preventDefault();
    incrPhone += 1;
    let attr = `phone-number-${incrPhone}`;
    let val = `phone-${incrPhone}`;
    let html = `<div class="row ${incrPhone}">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-10">
                                <input name="telephone[${val}]" type="tel" pattern="[0-9]{10}" required class="validate form-control mt10" data-error=".telephone_error">
                                <div class="telephone_error"></div>
                            </div>
                            <div class="col-md-2 phone-cross-button">
                                <a href="" class="btn btn-secondary phone-cross" data-phone=${incrPhone}>&times;</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="phone-number" id=${attr} value=${val} required>
                            <label class="form-check-label" for=${attr}>Primary Number</label>
                        </div>
                    </div>
                </div>`;
    $(".numbers").append(html);
    $(".numbers .phone-cross").removeClass("disabled");
});

addButtonEmail.click(function (e) {
    e.preventDefault();
    incrEmail += 1;
    let attr = `email-${incrEmail}`;
    let val = `email-${incrEmail}`;
    let html = `<div class="row ${incrEmail}">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-10">
                                <input name="email[${val}]" type="email" required class="validate form-control mt10" data-error=".email_error" data-email=${incrEmail}>
                                <div class="email_error"></div>
                            </div>
                            <div class="col-md-2 email-cross-button">
                                <button class="btn btn-secondary email-cross" data-email=${incrEmail}>&times;</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="email-id" id=${attr} value=${val} required>
                            <label class="form-check-label" for=${attr}>Primary Email</label>
                        </div>
                    </div>
                </div>`;
    $(".emails").append(html);
    $(".emails .email-cross").removeClass("disabled");
});

(function () {
    if ($("#birthdate")[0]) {
        let field = $("#birthdate")[0];
        var date = new Date();
        field.value = date.getFullYear().toString() + '-' + (date.getMonth() + 1).toString().padStart(2, 0) + '-' + date.getDate().toString().padStart(2, 0);
    }
})();

$(".numbers").on("click", ".phone-cross", function (e) {
    e.preventDefault();
    let phone = $(this).attr("data-phone");
    $(this).closest(`.row.${phone}`).remove();
    if ($(".numbers input[type=tel]").length == 1) {
        $(".numbers .phone-cross").addClass("disabled");
    }
});

$(".emails").on("click", ".email-cross", function (e) {
    e.preventDefault();
    let email = $(this).attr("data-email");
    $(this).closest(`.row.${email}`).remove();
    if ($(".emails input[type=email]").length == 1) {
        $(".emails .email-cross").addClass("disabled");
    }
});
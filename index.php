<?php
include_once './includes/functions.inc.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact Book</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <!--Custom CSS-->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center bg-dark">
                    <h3 class="p20 text-light">Contact Book</h3>
                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-right">
                <div class="row">
                    <div class="col-md-10">
                        <div class="alert alert-success alert-message text-center mt15 d-none" role="alert"></div>
                    </div>
                    <div class="col-md-2">
                        <a href="add-contact.php" class="btn btn-primary add-button">Add Contact</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Table-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="contacts-section mt20">
                    <?php
                    if (isset($_GET['page'])) {
                        $page = $_GET['page'];
                    } else {
                        $page = 1;
                    }
                    $no_of_records_per_page = 3;
                    $start = ($page - 1) * $no_of_records_per_page;
                    $sql = "SELECT COUNT(*) AS total FROM contacts";
                    $rows = db_select($sql);
                    $total_rows = $rows[0]['total'];
                    $total_pages = ceil($total_rows / $no_of_records_per_page);
                    if ($page > $total_pages || $page < 1) {
                        echo "No Records!!!";
                    }

                    $sql = "SELECT * FROM contacts LIMIT $start, $no_of_records_per_page";
                    $rows = db_select($sql);
                    if ($rows === false) {
                        $error = db_error();
                        dd($error);
                    }
                    foreach ($rows as $row) :
                    ?>
                        <div class="row text-center p20">
                            <div class="col-md-1">
                                <button class="edit-button btn btn-primary" data-id="<?= $row['id']; ?>">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button class="delete-button btn btn-danger" data-id="<?= $row['id']; ?>" data-toggle="modal" data-target="#delete-modal">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                            <div class="col-md-2">
                                <h5>First Name</h5>
                                <p><?= $row['first_name']; ?></p>
                            </div>
                            <div class="col-md-2">
                                <h5>Last Name</h5>
                                <p><?= $row['last_name']; ?></p>
                            </div>
                            <div class="col-md-2">
                                <h5>Birth Date</h5>
                                <p><?= $row['birthdate']; ?></p>
                            </div>
                            <div class="col-md-2">
                                <h5>Phone Number</h5>
                                <ul>
                                    <?php
                                    $id = $row['id'];
                                    $sql = "SELECT * FROM phone_number where contact_id = $id";
                                    $mobile_numbers = db_select($sql);
                                    if (empty($mobile_numbers)) :
                                    ?>
                                        <li>-</li>
                                        <?php
                                    endif;
                                    foreach ($mobile_numbers as $mobile_number) :
                                        if ($mobile_number['primary_number'] == 1) :
                                        ?>
                                            <li><span class="badge badge-pill badge-primary">Primary</span><?= $mobile_number['phone']; ?></li>
                                        <?php
                                        else :
                                        ?>
                                            <li><?= $mobile_number['phone']; ?></li>
                                    <?php
                                        endif;
                                    endforeach;
                                    ?>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <h5>Email ID</h5>
                                <ul>
                                    <?php
                                    $id = $row['id'];
                                    $sql = "SELECT * FROM email_id where contact_id = $id";
                                    $email_ids = db_select($sql);
                                    if (empty($email_ids)) :
                                    ?>
                                        <li>-</li>
                                        <?php
                                    endif;
                                    foreach ($email_ids as $email_id) :
                                        if ($email_id['primary_email'] == 1) :
                                        ?>
                                            <li><span class="badge badge-pill badge-primary">Primary</span><?= $email_id['email']; ?></li>
                                        <?php
                                        else :
                                        ?>
                                            <li><?= $email_id['email']; ?></li>
                                    <?php
                                        endif;
                                    endforeach;
                                    ?>
                                </ul>
                            </div>
                        </div>
                    <?php
                    endforeach;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!--Table-->
    <!--Pagination-->
    <nav aria-label="Page navigation">
        <ul class="pagination">
            <li class="page-item <?= $page <= 1 ? 'disabled' : ''; ?>">
                <a class="page-link" href="<?= $page <= 1 ? '#' : '?page=' . ($page - 1); ?>" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
            <?php
            for ($i = 1; $i <= $total_pages; $i++) :
            ?>
                <li class="page-item <?= $i == $page ? 'active' : ''; ?>">
                    <a class="page-link" href="index.php?page=<?= $i; ?>"><?= $i; ?></a>
                </li>
            <?php
            endfor;
            ?>
            <li class="page-item <?= $page == $total_pages ? 'disabled' : ''; ?>">
                <a class="page-link" href="<?= $page >= $total_pages ? '#' : '?page=' . ($page + 1); ?>" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </nav>
    <!--/Pagination-->
    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center bg-dark">
                    <h4 class="p20 text-light">&copy; Keval Sanghvi</h4>
                </div>
            </div>
        </div>
    </footer>
    <!--Modal-->
    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Contact Book</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this contact?</p>
                </div>
                <div class="modal-footer">
                    <a href="#" type="button" class="btn btn-secondary" data-dismiss="modal">No</a>
                    <a href="#" type="button" id="modal-agree-button" class="btn btn-primary">Yes</a>
                </div>
            </div>
        </div>
    </div>
    <!--/Modal-->
    <!-- JQuery -->
    <script src="js/jquery.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!--Home JS-->
    <script src="js/home.js"></script>
    <script>
        <?php
        $q = "";
        $op = "";
        if (isset($_GET['q'])) {
            $q = $_GET['q'];
        }
        if (isset($_GET['op'])) {
            $op = $_GET['op'];
        }
        if ($q == 'success' && $op == 'delete') :
        ?>
            $(".alert-message").text("Successfully Deleted!!!");
            $(".alert-message").removeClass("d-none");
            $(".alert-message").fadeOut(5000);
        <?php
        endif;
        ?>
    </script>
</body>

</html>
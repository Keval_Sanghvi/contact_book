<?php
include_once "includes/functions.inc.php";
$error1 = false;
$error2 = false;
$error3 = false;
if (isset($_POST['action'])) {
    $first_name = sanitizeData($_POST['first_name']);
    $last_name = sanitizeData($_POST['last_name']);
    $birthdate = sanitizeData($_POST['birthdate']);
    $birthdate = date('Y-m-d', strtotime($birthdate));
    if (isset($_POST['phone-number'])) {
        $primary_number = sanitizeData($_POST['phone-number']);
    }
    if (isset($_POST['email-id'])) {
        $primary_email = sanitizeData($_POST['email-id']);
    }
    $telephone = $_POST['telephone'];
    $my_telephone = array();
    if (is_array($telephone)) {
        foreach ($telephone as $key => $val) {
            $my_telephone[$key] = sanitizeData($val);
        }
    }
    foreach ($my_telephone as $key => $val) {
        if ($val == "") {
            unset($my_telephone[$key]);
        }
    }
    $email = $_POST['email'];
    $my_email = array();
    if (is_array($my_email)) {
        foreach ($email as $key => $val) {
            $my_email[$key] = sanitizeData($val);
        }
    }
    foreach ($my_email as $key => $val) {
        if ($val == "") {
            unset($my_email[$key]);
        }
    }
    $column_list_contacts = "first_name, last_name, birthdate";
    $value_list_contacts = "'$first_name', '$last_name', '$birthdate'";
    $query = "INSERT INTO contacts($column_list_contacts) VALUES($value_list_contacts)";
    $result = db_query($query);
    if (!$result) {
        $error1 = true;
    }
    $contact_id = db_last_id();
    if (!isset($primary_number)) {
        $primary_number = array_key_first($my_telephone);
    }
    foreach ($my_telephone as $key => $val) {
        $primary = 0;
        if ($key == $primary_number) {
            $primary = 1;
        }
        $column_list_phone_number = "contact_id, phone, primary_number";
        $value_list_phone_number = "'$contact_id', '$val', '$primary'";
        $query = "INSERT INTO phone_number($column_list_phone_number) VALUES($value_list_phone_number)";
        $result = db_query($query);
        if (!$result) {
            $error2 = true;
        }
    }
    if (!isset($primary_email)) {
        $primary_email = array_key_first($my_email);
    }
    foreach ($my_email as $key => $val) {
        $primary = 0;
        if ($key == $primary_email) {
            $primary = 1;
        }
        $column_list_email_id = "contact_id, email, primary_email";
        $value_list_email_id = "'$contact_id', '$val', '$primary'";
        $query = "INSERT INTO email_id($column_list_email_id) VALUES($value_list_email_id)";
        $result = db_query($query);
        if (!$result) {
            $error3 = true;
        }
    }
    if (!$error1 and !$error2 and !$error3) {
        header('Location: index.php?q=success&op=insert');
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact Book</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <!--Custom CSS-->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center bg-dark">
                    <h3 class="p20 text-light">Contact Book</h3>
                </div>
                <?php
                if ($error1 or $error2 or $error3) :
                ?>
                    <div class="col-md-12 m20">
                        <div class="alert alert-danger" role="alert">
                            Error! Not Inserted!
                        </div>
                    </div>
                <?php
                endif;
                ?>
            </div>
        </div>
    </header>
    <div class="container mt30">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" id="add-contact-form" method="POST" onsubmit="return validateForm()">
                    <div class="form-group">
                        <label for="first_name">First Name <span class="compulsory">*</span></label>
                        <input type="text" class="form-control validate" id="first_name" name="first_name" data-error=".first_name_error">
                        <div class="first_name_error"></div>
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name <span class="compulsory">*</span></label>
                        <input type="text" class="form-control validate" id="last_name" name="last_name" data-error=".last_name_error">
                        <div class="last_name_error"></div>
                    </div>
                    <div class="form-group">
                        <label for="birthdate">Birthdate <span class="compulsory">*</span></label>
                        <input id="birthdate" name="birthdate" type="date" class="datepicker form-control" data-error=".birthday_error">
                        <div class="birthday_error"></div>
                    </div>
                    <div class="form-group numbers">
                        <label for="telephone">Telephone <span class="compulsory">*</span></label>
                        <a href="" class="btn btn-primary plus-number">+</a>
                        <div class="row 0">
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-10">
                                        <input name="telephone[phone-0]" type="tel" class="validate form-control mt10" data-error=".telephone_error">
                                        <div class="telephone_error"></div>
                                    </div>
                                    <div class="col-md-2 phone-cross-button">
                                        <a href="" class="btn btn-secondary phone-cross disabled" data-phone="0">&times;</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-check">
                                    <input class="form-check-input" required type="radio" name="phone-number" id="phone-number-0" checked value="phone-0">
                                    <label class="form-check-label" for="phone-number-0">Primary Number</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group emails">
                        <label for="email">Email <span class="compulsory">*</span></label>
                        <a href="" class="btn btn-primary plus-email">+</a>
                        <div class="row 0">
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-10">
                                        <input name="email[email-0]" type="email" class="validate form-control mt10" data-error=".email_error">
                                        <div class="email_error"></div>
                                    </div>
                                    <div class="col-md-2 email-cross-button">
                                        <a href="" class="btn btn-secondary email-cross disabled" data-email="0">&times;</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-check">
                                    <input class="form-check-input" required type="radio" name="email-id" id="email-0" checked value="email-0">
                                    <label class="form-check-label" for="email-0">Primary Email</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-secondary pl20 pr20" name="action">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <footer class="mt30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center bg-dark">
                    <h4 class="p20 text-light">&copy; Keval Sanghvi</h4>
                </div>
            </div>
        </div>
    </footer>
</body>
<!-- JQuery -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="js/bootstrap/bootstrap.min.js"></script>
<!-- Add JS -->
<script src="js/add.js"></script>
<!-- Validate JS -->
<script src="js/validate.js"></script>
<!--Custom JS -->
<script src="js/custom.js"></script>

</html>
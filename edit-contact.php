<?php
include_once "includes/functions.inc.php";
$error1 = false;
$error2 = false;
$error3 = false;
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $query = "SELECT * FROM contacts WHERE id = $id";
    $rows = db_select($query);
    if (count($rows) === 0) {
        dd("Cannot find resource having id: {$id}");
    }
    $row = db_select($query)[0];
    $query = "SELECT * FROM phone_number WHERE contact_id = $id";
    $rows = db_select($query);
    if (count($rows) === 0) {
        dd("Cannot find resource having id: {$id}");
    }
    $row1 = db_select($query);
    $query = "SELECT * FROM email_id WHERE contact_id = $id";
    $rows = db_select($query);
    if (count($rows) === 0) {
        dd("Cannot find resource having id: {$id}");
    }
    $row2 = db_select($query);
} else if (isset($_POST['action'])) {
    $id = $_POST['id'];
    $query = "SELECT * FROM contacts WHERE id = $id";
    $rows = db_select($query);
    if (count($rows) === 0) {
        dd("Cannot find resource having id: {$id}");
    }
    $first_name = sanitizeData($_POST['first_name']);
    $last_name = sanitizeData($_POST['last_name']);
    $birthdate = sanitizeData($_POST['birthdate']);
    $birthdate = date('Y-m-d', strtotime($birthdate));
    if (isset($_POST['phone-number'])) {
        $primary_number = sanitizeData($_POST['phone-number']);
    }
    if (isset($_POST['email-id'])) {
        $primary_email = sanitizeData($_POST['email-id']);
    }
    $telephone = $_POST['telephone'];
    $my_telephone = array();
    if (is_array($telephone)) {
        foreach ($telephone as $key => $val) {
            $my_telephone[$key] = sanitizeData($val);
        }
    }
    foreach ($my_telephone as $key => $val) {
        if ($val == "") {
            unset($my_telephone[$key]);
        }
    }
    $email = $_POST['email'];
    $my_email = array();
    if (is_array($my_email)) {
        foreach ($email as $key => $val) {
            $my_email[$key] = sanitizeData($val);
        }
    }
    foreach ($my_email as $key => $val) {
        if ($val == "") {
            unset($my_email[$key]);
        }
    }
    $query = "UPDATE contacts SET first_name = '${first_name}', last_name = '${last_name}', birthdate = '${birthdate}' WHERE id={$id}";
    $result = db_query($query);
    if (!$result) {
        $error1 = true;
    }
    $query = "DELETE FROM phone_number WHERE contact_id = $id";
    $result = db_query($query);
    if (!$result) {
        dd("Error!!!");
    }
    $query = "DELETE FROM email_id WHERE contact_id = $id";
    $result = db_query($query);
    if (!$result) {
        dd("Error!!!");
    }
    if (!isset($primary_number)) {
        $primary_number = array_key_first($my_telephone);
    }
    foreach ($my_telephone as $key => $val) {
        $primary = 0;
        if ($key == $primary_number) {
            $primary = 1;
        }
        $column_list_phone_number = "contact_id, phone, primary_number";
        $value_list_phone_number = "'$id', '$val', '$primary'";
        $query = "INSERT INTO phone_number($column_list_phone_number) VALUES($value_list_phone_number)";
        $result = db_query($query);
        if (!$result) {
            $error2 = true;
        }
    }
    if (!isset($primary_email)) {
        $primary_email = array_key_first($my_email);
    }
    foreach ($my_email as $key => $val) {
        $primary = 0;
        if ($key == $primary_email) {
            $primary = 1;
        }
        $column_list_email_id = "contact_id, email, primary_email";
        $value_list_email_id = "'$id', '$val', '$primary'";
        $query = "INSERT INTO email_id($column_list_email_id) VALUES($value_list_email_id)";
        $result = db_query($query);
        if (!$result) {
            $error3 = true;
        }
    }
    if (!$error1 and !$error2 and !$error3) {
        header('Location: index.php?q=success&op=edit');
    }
} else {
    dd('You cannot access this page!');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact Book</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <!--Custom CSS-->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center bg-dark">
                    <h3 class="p20 text-light">Contact Book</h3>
                </div>
            </div>
        </div>
    </header>
    <div class="container mt30">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" id="edit-contact-form" method="POST" onsubmit="return validateForm()">
                    <input type="hidden" name="id" id="contact_id" value="<?= $row['id']; ?>" readonly>
                    <div class="form-group">
                        <label for="first_name">First Name <span class="compulsory">*</span></label>
                        <input type="text" class="form-control validate" id="first_name" name="first_name" value=<?= $row['first_name']; ?> data-error=".first_name_error">
                        <div class="first_name_error"></div>
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name <span class="compulsory">*</span></label>
                        <input type="text" class="form-control validate" id="last_name" name="last_name" value=<?= $row['last_name']; ?> data-error=".last_name_error">
                        <div class="last_name_error"></div>
                    </div>
                    <div class="form-group">
                        <label for="birthdate">Birthdate <span class="compulsory">*</span></label>
                        <input id="birthdate" name="birthdate" type="date" value=<?= $row['birthdate']; ?> class="datepicker form-control" data-error=".birthday_error">
                        <div class="birthday_error"></div>
                    </div>
                    <div class="form-group numbers">
                        <label for="telephone">Telephone <span class="compulsory">*</span></label>
                        <a href="" class="btn btn-primary plus-number">+</a>
                        <?php
                        $inc = 0;
                        foreach ($row1 as $r) :
                        ?>
                            <div class="row <?= $inc; ?>">
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <input name="telephone[phone-<?= $inc; ?>]" type="tel" pattern="[0-9]{10}" class="validate form-control mt10" data-error=".telephone_error" value=<?= $r['phone']; ?>>
                                            <div class="telephone_error"></div>
                                        </div>
                                        <div class="col-md-2 phone-cross-button">
                                            <a href="" class="btn btn-secondary phone-cross" data-phone=<?= $inc; ?>>&times;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-check">
                                        <input class="form-check-input" required type="radio" name="phone-number" id="phone-number-<?= $inc; ?>" value="phone-<?= $inc; ?>" <?php
                                                                                                                                                                            if ($r['primary_number'] == 1) :
                                                                                                                                                                                echo "checked";
                                                                                                                                                                            endif;
                                                                                                                                                                            ?>>
                                        <label class="form-check-label" for="phone-number-<?= $inc; ?>">Primary Number</label>
                                    </div>
                                </div>
                            </div>
                        <?php
                            $inc = $inc + 1;
                        endforeach;
                        ?>
                    </div>
                    <div class="form-group emails">
                        <label for="email">Email <span class="compulsory">*</span></label>
                        <a href="" class="btn btn-primary plus-email">+</a>
                        <?php
                        $inc = 0;
                        foreach ($row2 as $r) :
                        ?>
                            <div class="row <?= $inc; ?>">
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <input name="email[email-<?= $inc; ?>]" type="email" class="validate form-control mt10" data-error=".email_error" value=<?= $r['email']; ?>>
                                            <div class="email_error"></div>
                                        </div>
                                        <div class="col-md-2 email-cross-button">
                                            <a href="" class="btn btn-secondary email-cross" data-email="<?= $inc; ?>">&times;</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-check">
                                        <input class="form-check-input" required type="radio" name="email-id" id="email-<?= $inc; ?>" value="email-<?= $inc; ?>" <?php
                                                                                                                                                                if ($r['primary_email'] == 1) :
                                                                                                                                                                    echo "checked";
                                                                                                                                                                endif;
                                                                                                                                                                ?>>
                                        <label class="form-check-label" for="email-<?= $inc; ?>">Primary Email</label>
                                    </div>
                                </div>
                            </div>
                        <?php
                            $inc = $inc + 1;
                        endforeach;
                        ?>
                    </div>
                    <button type="submit" class="btn btn-secondary pl20 pr20" name="action">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <footer class="mt30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center bg-dark">
                    <h4 class="p20 text-light">&copy; Keval Sanghvi</h4>
                </div>
            </div>
        </div>
    </footer>
</body>
<!-- JQuery -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="js/bootstrap/bootstrap.min.js"></script>
<!-- Add JS -->
<script src="js/add1.js"></script>
<!-- Validate JS -->
<script src="js/validate.js"></script>
<!--Custom JS -->
<script src="js/custom.js"></script>

</html>